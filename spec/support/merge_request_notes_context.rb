# frozen_string_literal: true

RSpec.shared_context 'with merge request notes' do
  let(:project_id) { 123 }
  let(:merge_request_iid) { 456 }

  let(:merge_request_notes) do
    [
      { body: 'review comment 1' },
      { body: 'review comment 2' }
    ]
  end

  let(:comment_mark) do
    subject.__send__(:unique_comment).__send__(:hidden_comment)
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)
  end
end
