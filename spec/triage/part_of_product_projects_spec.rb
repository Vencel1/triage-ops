# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/part_of_product_projects'

RSpec.describe Triage::PartOfProductProjects, :clean_cache do
  let(:com_csv_body) do
    <<~CSV
      project_path,project_id
      foo/bar,1
    CSV
  end
  let(:ops_csv_body) do
    <<~CSV
      project_path,project_id
      foo/bar,42
    CSV
  end
  let(:csv_bodies) do
    {
      com: com_csv_body,
      ops: ops_csv_body
    }
  end

  shared_examples 'part_of_product projects method' do |instance_key, expected_result|
    before do
      csv_bodies.each do |instance_key, body|
        allow(HTTParty).to receive(:get).with(described_class::CSV_URLS[instance_key]).and_return(double(parsed_response: csv_bodies[instance_key]))
      end
    end

    it 'returns the content of the CSV file as a hash' do
      result = described_class.part_of_product_projects(instance_key)

      expect(result).to eq(expected_result)
    end

    it 'caches the CSV URL response' do
      described_class.part_of_product_projects(instance_key)

      expect(HTTParty).not_to receive(:get)

      described_class.part_of_product_projects(instance_key)
    end

    it 'expires the cache and retrieves' do
      described_class.part_of_product_projects(instance_key)

      # expire cache
      Timecop.travel(Time.now + described_class::CSV_CACHE_EXPIRY + 1) do
        expect(HTTParty).to receive(:get)

        described_class.part_of_product_projects(instance_key)
      end
    end
  end

  describe '.part_of_product_com_projects' do
    it_behaves_like 'part_of_product projects method', :com, [{ 'project_path' => 'foo/bar', 'project_id' => 1 }]
  end

  describe '.part_of_product_ops_projects' do
    it_behaves_like 'part_of_product projects method', :ops, [{ 'project_path' => 'foo/bar', 'project_id' => 42 }]
  end
end
