# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../job/type_label_nudger_job'

module Triage
  class TypeLabelNudger < Processor
    FIVE_MINUTES                 = 300
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.resource_open? &&
        event.from_part_of_product_project? &&
        !community_contribution? &&
        (need_to_nudge? || need_to_resolve_nudge?)
    end

    # We initially give the author a few minutes to add a type and subtype label before we post a reminder.
    #
    # Resolving/unresolving discussions, on the other hand, is instantaneous.
    def process
      if need_to_nudge?
        if unique_comment.previous_discussion && !type_label_present?
          unresolve_discussion(unique_comment.previous_discussion['id'])
        else
          TypeLabelNudgerJob.perform_in(FIVE_MINUTES, event)
        end
      elsif need_to_resolve_nudge?
        resolve_discussion(unique_comment.previous_discussion['id'])
      end
    end

    def documentation
      <<~TEXT
        Reminds the MR creator to add a type and subtype label to the MR.
      TEXT
    end

    private

    def community_contribution?
      event.label_names.include?(COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      (!type_label_present? || !subtype_label_present?) &&
        (!unique_comment.previous_discussion_comment || unique_comment.previous_discussion_comment['resolved'])
    end

    def need_to_resolve_nudge?
      type_label_present? &&
        subtype_label_present? &&
        unique_comment.previous_discussion_comment &&
        !unique_comment.previous_discussion_comment['resolved']
    end

    def type_label_present?
      event.label_names.any? { |label| label.start_with?('type::') }
    end

    def subtype_label_present?
      event.label_names.any? { |label| label.start_with?('bug::', 'feature::', 'maintenance::') }
    end
  end
end
