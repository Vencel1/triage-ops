# frozen_string_literal: true

require 'rack'
require 'rack/requestid'

module Triage
  module Rack
    # Inspired by https://github.com/eropple/rack-ougai/blob/master/lib/rack/ougai/attach_requestid.rb
    class AttachRequestId < Struct.new(:app)
      def call(env)
        parent = env[::Rack::RACK_LOGGER]

        request_id = env[::Rack::RequestID::REQUEST_ID_KEY]

        if request_id.nil?
          parent.warn "No request ID in storage (is Rack::RequestID in your middleware stack?)."
        end

        env[::Rack::RACK_LOGGER] = parent.child(request_id: request_id)
        app.call(env).tap do
          env[::Rack::RACK_LOGGER] = parent
        end
      end
    end
  end
end
