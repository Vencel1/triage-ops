# frozen_string_literal: true

require_relative 'team_member_select_helper'

module TeamMemberHelper
  include TeamMemberSelectHelper

  def has_specialty?(username, specialty)
    select_team_members_by_department_specialty_role(nil, specialty, include_ooo: true).include?("@#{username}")
  end
end
