# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../stage_definition'

module Generate
  module StagePolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      StageDefinition::STAGE_DATA.each do |name, definition|
        next if options.only && !options.only.include?(name)

        stage_method_name = "stage_#{name}".tr('-', '_')
        stage = StageDefinition.public_send(stage_method_name, options.assign)

        File.write(
          "#{destination}/#{template_name}/#{name}.yml",
          erb.result_with_hash(
            stage_method_name: stage_method_name,
            stage_label_name: stage.dig(:labels, 0),
            assignees: stage[:assignees],
            groups: stage[:groups]
          )
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
